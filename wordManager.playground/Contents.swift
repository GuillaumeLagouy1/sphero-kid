import Cocoa
/*
let wordToFind:[String] = ["C","H","A","T"]
let lettersFound:[String] = ["C", "T"]
var missingLetters:[String] = []



func buildWordForView(wordToFind:[String], lettersFound:[String]) -> [String]
{
    var wordForView:[String] = []
    for letter in wordToFind {
        if lettersFound.contains(letter){
            wordForView.append(letter)
        } else {
            wordForView.append("missingLetter")
        }
    }
    return wordForView
}

func getMissingLetters(wordToFind:[String], lettersFound:[String]) -> [String]
{
    var missingLetters:[String] = []
    for letter in wordToFind {
        if !lettersFound.contains(letter){
            missingLetters.append(letter)
        }
    }
    return missingLetters
}

func generateLettersToChoice(missingLetters:[String], count:Int) -> [String] {
    
    var lettersToChoice:[String] = []
    for letter in missingLetters {
        lettersToChoice.append(letter)
    }
    
    for n in 1...count {
        lettersToChoice.append(randomLetter())
    }
    
    return lettersToChoice.shuffled()
}

func randomLetter() -> String {
    let letters = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z" ]
    return letters[Int(arc4random_uniform(26) + 1)]
}

let wordForView = buildWordForView(wordToFind: wordToFind, lettersFound: lettersFound)
//print(wordForView.count)
missingLetters = getMissingLetters(wordToFind: wordToFind, lettersFound: lettersFound)
let lettersToChoice = generateLettersToChoice(missingLetters: missingLetters, count: 3)
print(lettersToChoice)
*/

class Shape {
    enum Types {
        case circle, square, triangle
    }
    let type:Types
    
    let typeDictonary:[Types:String] = [.circle:"cercle", .square:"carré", .triangle:"triangle"]
    
    init(type:Types) {
        self.type = type
    }
    
    func getTraduction() -> String {
        return typeDictonary[self.type]!
    }
}

class Color {
    enum Types {
        case red, green, blue
    }
    let type:Types
    
    let typeDictonary:[Types:String] = [.red:"rouge", .green:"vert", .blue:"bleu"]
    
    init(type:Types) {
        self.type = type
    }
    
    func getTraduction() -> String {
        return typeDictonary[self.type]!
    }
}

class DrawingPart {
    let shape:Shape
    let color:Color
    
    init(shape:Shape, color:Color) {
        self.color = color
        self.shape = shape
    }
}

let shape1 = Shape(type: .circle)
let color1 = Color(type: .red)

let shape2 = Shape(type: .square)
let color2 = Color(type: .green)

let dp1 = DrawingPart(shape: shape1, color: color1)
let dp2 = DrawingPart(shape: shape2, color: color2)

let drawing = [dp1, dp2]


