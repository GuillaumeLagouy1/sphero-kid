//
//  SocketManager.swift
//  SpheroKid
//
//  Created by Guillaume Lagouy on 25/11/2019.
//  Copyright © 2019 Guillaume Lagouy. All rights reserved.
//

import Foundation
import SocketIO

class SocketSwiftManager {
    static let instance = SocketSwiftManager()
    
    var manager:SocketManager? = nil
    var socket:SocketIOClient? = nil
    
    func connect(localhost:String, port:String) {
        let url:URL = URL(string: "http://\(localhost):\(port)")!
        self.manager = SocketManager(socketURL: url, config: [.log(false), .compress])
        socket = manager?.defaultSocket
        socket?.connect()
        print("Socket connectée")
    }
    
    func emit(channel:String, datas:SocketData...){
        socket?.emit(channel, datas[0])
    }
    
    func listenToChannel(channel:String, callback:@escaping (String?)->()) {
        socket?.on(channel) {data, ack in
            
            if let d = data.first,
                let dataStr = d as? String {
                callback(dataStr)
            }else{
                callback(nil)
            }
            
            
            ack.with("Got your currentAmount", "dude")
        }
    }
}
