//
//  WordManager.swift
//  SpheroKid
//
//  Created by Guillaume Lagouy on 25/11/2019.
//  Copyright © 2019 Guillaume Lagouy. All rights reserved.
//

import Foundation

class WordManager {
    static let instance = WordManager()
    
    
    
    func buildWordForView(wordToFind:[String], lettersFound:[String]) -> [String] {
        var wordForView:[String] = []
        for letter in wordToFind {
            if lettersFound.contains(letter){
                wordForView.append(letter)
            } else {
                wordForView.append("missingLetter")
            }
        }
        return wordForView
    }
    
    func getMissingLetters(wordToFind:[String], lettersFound:[String]) -> [String] {
        var missingLetters:[String] = []
        for letter in wordToFind {
            if !lettersFound.contains(letter){
                missingLetters.append(letter)
            }
        }
        return missingLetters
    }
    
    func generateLettersToChoice(missingLetters:[String], count:Int) -> [String] {
        var lettersToChoice:[String] = []
        for letter in missingLetters {
            lettersToChoice.append(letter)
        }
        
        for _ in 1...count {
            lettersToChoice.append(randomLetter())
        }
        
        return lettersToChoice.shuffled()
    }
    
    func randomLetter() -> String {
        let letters = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z" ]
        return letters[Int(arc4random_uniform(25) + 0)]
    }
    
}
