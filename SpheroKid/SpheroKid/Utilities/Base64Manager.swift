//
//  Base64Manager.swift
//  SpheroKid
//
//  Created by Guillaume Lagouy on 24/11/2019.
//  Copyright © 2019 Guillaume Lagouy. All rights reserved.
//

import Foundation
import UIKit

//
// Convert UIImage to a base64 representation
//
public func convertImageToBase64(image: UIImage) -> String {
    let imageData = image.pngData()!
    return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
}

//
// Convert a base64 representation to a UIImage
//
public func convertBase64ToImage(imageString: String) -> UIImage {
    let imageData = Data(base64Encoded: imageString, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)!
    return UIImage(data: imageData)!
}
