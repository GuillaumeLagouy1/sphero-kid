//
//  Sound.swift
//  SpheroKid
//
//  Created by Guillaume Lagouy on 27/11/2019.
//  Copyright © 2019 Guillaume Lagouy. All rights reserved.
//

import Foundation
import AVFoundation

var player: AVAudioPlayer?

public func playSound(sound:String) {
    guard let url = Bundle.main.url(forResource: sound, withExtension: "wav") else { return }

    do {
        try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
        try AVAudioSession.sharedInstance().setActive(true)

        /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
        player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)

        /* iOS 10 and earlier require the following line:
        player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */

        guard let player = player else { return }

        player.play()

    } catch let error {
        print(error.localizedDescription)
    }
}
