//
//  SpheroLedsViewController.swift
//  SparkPerso
//
//  Created by AL on 01/09/2019.
//  Copyright © 2019 AlbanPerli. All rights reserved.
//

import UIKit

public class LedManager {
    static let instance = LedManager()
    
    let frame1:Array<Array<Int>> = [[1,1],[1,2],[1,5],[1,6],[2,1],[2,2],[2,5],[2,6],[5,1],[5,2],[5,3],[5,4],[5,5],[5,6]]
    let frame2:Array<Array<Int>> = [[1,1],[1,2],[1,5],[1,6],[2,1],[2,2],[2,5],[2,6],[4,3],[4,4],[5,2],[5,5],[6,3],[6,4]]
    
    let square:Array<Array<Int>> = [[1,1],[1,2],[1,3],[1,4],[1,5],[1,6],[2,1],[2,6],[3,1],[3,6],[4,1],[4,6],[5,1],[5,6],[6,1],[6,2],[6,3],[6,4],[6,5],[6,6]]
    let triangle:Array<Array<Int>> = [[0,0],[1,0],[1,1],[2,0],[2,2],[3,0],[3,3],[4,0],[4,4],[5,0],[5,5],[6,0],[6,6],[7,0],[7,1],[7,2],[7,3],[7,4],[7,5],[7,6],[7,7]]
    
    let sad:Array<Array<Int>> = [[2,2],[2,5],[4,2],[4,3],[4,4],[4,5],[5,1],[5,6]]
    let happy:Array<Array<Int>> = [[2,2],[2,5],[4,1],[4,6],[5,2],[5,3],[5,4],[5,5]]
    
    var frames:Array<Frame> = []
    var nbFrame:Int = 0
    
    var correctRotate:Double = 0
    var correctSpheroCount = 0
    let correctDelay:Float = 0.05
    
     class Frame {
         var pixels:Array<Array<Int>>
         
         init(pixels:Array<Array<Int>>){
             self.pixels = pixels
         }
     }
    
    
    public func initLedSphero() {
        SharedToyBox.instance.bolt?.setStabilization(state: SetStabilization.State.off)
        SharedToyBox.instance.bolt?.setFrontLed(color: .white)
        SharedToyBox.instance.bolt?.setBackLed(color: .white)
    }
    
    public func rotateSphero(type:String, shape:Array<Array<Int>>? = nil, color:UIColor? = nil) {
        
        let wrongRotate:Double = 40
        let wrongDelay:Float = 0.2
        SharedToyBox.instance.bolt?.setStabilization(state: SetStabilization.State.off)
        
        if(type == "wrong") {
            SharedToyBox.instance.bolt?.setFrontLed(color: .red)
            SharedToyBox.instance.bolt?.setBackLed(color: .red)
            displayToScreen(pixels: sad, color: .red)
            delayWithSeconds(1) {
                SharedToyBox.instance.bolt?.rotateAim(wrongRotate)
                delayWithSeconds(Double(wrongDelay)) {
                    SharedToyBox.instance.bolt?.rotateAim(-wrongRotate)
                    delayWithSeconds(Double(wrongDelay)) {
                        SharedToyBox.instance.bolt?.rotateAim(wrongRotate)
                        delayWithSeconds(Double(wrongDelay)) {
                            SharedToyBox.instance.bolt?.rotateAim(-wrongRotate)
                            delayWithSeconds(Double(wrongDelay)) {
                                SharedToyBox.instance.bolt?.rotateAim(wrongRotate)
                                delayWithSeconds(Double(wrongDelay)) {
                                    SharedToyBox.instance.bolt?.rotateAim(-wrongRotate)
                                    SharedToyBox.instance.bolt?.setStabilization(state: SetStabilization.State.off)
                                    delayWithSeconds(0.4) {
                                        self.resetSpheroLed()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else {
            SharedToyBox.instance.bolt?.clearMatrix()
            SharedToyBox.instance.bolt?.setFrontLed(color: .green)
            SharedToyBox.instance.bolt?.setBackLed(color: .green)
            displayToScreen(pixels: happy, color: .green)
            delayWithSeconds(2) {
                self.correctSpheroLoop(nb: self.correctSpheroCount, shape: shape, color: color)
            }
        }
    }
    
    public func resetSpheroLed() {
        SharedToyBox.instance.bolt?.setFrontLed(color: .white)
        SharedToyBox.instance.bolt?.setBackLed(color: .white)
        SharedToyBox.instance.bolt?.clearMatrix()
    }
    
    public func correctSpheroLoop(nb:Int, shape:Array<Array<Int>>? = nil, color:UIColor? = nil) {
        if(nb < 20) {
            correctSpheroCount += 1
            delayWithSeconds(Double(correctDelay)) {
                self.correctRotate += 45
                SharedToyBox.instance.bolt?.rotateAim(self.correctRotate)
                self.correctSpheroLoop(nb: self.correctSpheroCount, shape:shape, color:color)
            }
        }
        else {
            SharedToyBox.instance.bolt?.setStabilization(state: SetStabilization.State.off)
            //reset
            correctSpheroCount = 0
            correctRotate = 0
            delayWithSeconds(1) {
                self.resetSpheroLed()
                self.displayToScreen(pixels: shape!, color: color!)
            }
        }
    }
    
    public func displayToScreen(pixels: Array<Array<Int>>, color:UIColor) {
        for pixel in pixels {
            SharedToyBox.instance.bolt?.drawMatrix(pixel: Pixel(x: pixel[0], y: pixel[1]), color: color)
        }
    }
}
