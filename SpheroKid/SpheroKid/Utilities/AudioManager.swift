//
//  AudioManager.swift
//  SpheroKid
//
//  Created by Guillaume Lagouy on 28/11/2019.
//  Copyright © 2019 Guillaume Lagouy. All rights reserved.
//

import Foundation
import Speech

class AudioManager {
    static let audioInstance = AudioManager()
    
    var audioEngine = AVAudioEngine()

    let speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "fr"))!
    var request = SFSpeechAudioBufferRecognitionRequest()
    var recognitionTask: SFSpeechRecognitionTask?
    var isAlreadyStarted = false
    
    func start() {
        if !isAlreadyStarted {
            print("démarrage de l'enregistrement")
            let node = audioEngine.inputNode
            let recordingFormat = AVAudioFormat(standardFormatWithSampleRate: 44100, channels: 1)
            node.installTap(onBus: 0, bufferSize: 4096, format: recordingFormat) { buffer, _ in
                self.request.append(buffer)
            }
            
            audioEngine.prepare()
            
            do {
                try audioEngine.start()
            } catch {
                return print(error)
            }
            
            guard let myRecognizer = SFSpeechRecognizer() else {
                // A recognizer is not supported for the current locale
                return
            }
            if !myRecognizer.isAvailable {
                // A recognizer is not available right now
                return
            }
            isAlreadyStarted = true
       }
    }
    
    func stopRecording() {
        print("stop recording")
        
        /*
        audioEngine.inputNode.installTap(onBus: 0, bufferSize: 4096, format: audioEngine.inputNode.outputFormat(forBus: 0)) {_,_ in
            
        }*/
        // Cancel the previous task if it's running
        /*if let recognitionTask = recognitionTask {
            print("recognition stop")
            recognitionTask.cancel()
            self.recognitionTask = nil
        }*/
        
        audioEngine.stop()
        audioEngine.inputNode.reset()
        audioEngine.inputNode.removeTap(onBus: 0)
        recognitionTask?.finish()
        request.endAudio()
    }
    
    func recognizeSpeech(callBack:@escaping ([String])->()) {
        print("start recognition")
        request = SFSpeechAudioBufferRecognitionRequest()
        AudioManager.audioInstance.speechRecognizer.recognitionTask(with: self.request, resultHandler: { result, error in
           if let result = result {
               print("test")
               let bestString = result.bestTranscription.formattedString
               //print(bestString)
               self.recognitionTask = nil
                self.request.endAudio()
            callBack(result.bestTranscription.segments.map{ $0.substring.lowercased() })
           } else if let error = error {
               print(error)
           }
       })
    }
}
