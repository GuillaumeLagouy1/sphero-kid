//
//  Delay.swift
//  SpheroKid
//
//  Created by Guillaume Lagouy on 26/11/2019.
//  Copyright © 2019 Guillaume Lagouy. All rights reserved.
//

import Foundation

public func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
    DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
        completion()
    }
}
