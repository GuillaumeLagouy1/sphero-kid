//
//  PlayerManager.swift
//  SpheroKid
//
//  Created by Guillaume Lagouy on 15/11/2019.
//  Copyright © 2019 Guillaume Lagouy. All rights reserved.
//

import Foundation

public class PlayerManager {
    public static let sharedInstance = PlayerManager()
    
    var players:[Player] = []
    
    init() {}
    
    func addPlayer(player:Player) {
        players.append(player)
    }
}
