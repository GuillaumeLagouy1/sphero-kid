//
//  Role.swift
//  SpheroKid
//
//  Created by Guillaume Lagouy on 14/11/2019.
//  Copyright © 2019 Guillaume Lagouy. All rights reserved.
//

import Foundation

enum Role {
    case watcher, designer,
}
