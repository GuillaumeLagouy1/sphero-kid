//
//  DrawingPart.swift
//  SpheroKid
//
//  Created by Guillaume Lagouy on 26/11/2019.
//  Copyright © 2019 Guillaume Lagouy. All rights reserved.
//

import Foundation

class DrawingPart {
    let shape:Shape
    let color:Color
    
    init(shape:Shape, color:Color) {
        self.color = color
        self.shape = shape
    }
}
