//
//  Shape.swift
//  SpheroKid
//
//  Created by Guillaume Lagouy on 26/11/2019.
//  Copyright © 2019 Guillaume Lagouy. All rights reserved.
//

import Foundation

class Shape {
    enum Types {
        case circle, square, triangle
        
        var description : String {
          switch self {
          case .circle: return "circle"
          case .square: return "square"
          case .triangle: return "triangle"
          }
        }
    }
    let type:Types
    
    let typeDictonary:[Types:String] = [.circle:"cercle", .square:"carré", .triangle:"triangle"]
    
    init(type:Types) {
        self.type = type
    }
    
    func getTraduction() -> String {
        return typeDictonary[self.type]!
    }
    
    
}
