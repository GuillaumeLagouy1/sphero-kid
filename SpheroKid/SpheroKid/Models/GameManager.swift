//
//  GameManager.swift
//  SpheroKid
//
//  Created by Guillaume Lagouy on 26/11/2019.
//  Copyright © 2019 Guillaume Lagouy. All rights reserved.
//

import Foundation

public class GameManager {
    public static let sharedGame = GameManager()
    
    var team:[Player] = []
    var drawing:[DrawingPart] = []
    var wordToFind:String = ""
    var lettersFound:[String] = []
    var topic:String = ""
    var currentRound:Int = 1
    var recordBase64:String = ""
    var pictureBase64:String = ""
    
    init() {}
    
    func addPlayer(player:Player) {
        team.append(player)
    }
    
    func addDrawingParts(drawingParts:[DrawingPart]) {
        self.drawing = drawingParts
    }
    
    func addWordToFind(word:String) {
        self.wordToFind = word
    }
    
    func addLetterFound(letter:String) {
        lettersFound.append(letter)
    }
    
    func addTopic(topic:String) {
        self.topic = topic
    }
    
    func incrementRound(){
        self.currentRound += 1
    }
    
    func addPicture(picture:String){
        self.pictureBase64 = picture
    }
    
    func addRecord(record:String){
        self.recordBase64 = record
    }
    
    
    
    
    
}
