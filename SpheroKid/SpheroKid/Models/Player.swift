//
//  Player.swift
//  SpheroKid
//
//  Created by Guillaume Lagouy on 14/11/2019.
//  Copyright © 2019 Guillaume Lagouy. All rights reserved.
//

import Foundation

class Player : Codable{
    var firstname:String
    var avatar:String
    var record:String
    /*
    enum role {
        case watcher, designer, transmiter
    }*/
    
    init(firstname:String, avatar:String, record:String) {
        self.firstname = firstname
        self.avatar = avatar
        self.record = record
    }
}
