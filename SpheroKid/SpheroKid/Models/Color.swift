//
//  Color.swift
//  SpheroKid
//
//  Created by Guillaume Lagouy on 26/11/2019.
//  Copyright © 2019 Guillaume Lagouy. All rights reserved.
//

import Foundation

class Color {
    enum Types {
        case red, green, blue
        
        var description : String {
          switch self {
          case .red: return "red"
          case .green: return "green"
          case .blue: return "blue"
          }
        }
    }
    let type:Types
    
    let typeDictonary:[Types:String] = [.red:"rouge", .green:"vert", .blue:"bleu"]
    
    init(type:Types) {
        self.type = type
    }
    
    func getTraduction() -> String {
        return typeDictonary[self.type]!
    }
}
