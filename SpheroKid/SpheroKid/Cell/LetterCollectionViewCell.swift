//
//  LetterCollectionViewCell.swift
//  SpheroKid
//
//  Created by Guillaume Lagouy on 18/11/2019.
//  Copyright © 2019 Guillaume Lagouy. All rights reserved.
//

import UIKit

class LetterCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var letterImageView: UIImageView!
}
