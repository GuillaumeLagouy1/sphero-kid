//
//  ColorViewController.swift
//  SpheroKid
//
//  Created by Guillaume Lagouy on 24/11/2019.
//  Copyright © 2019 Guillaume Lagouy. All rights reserved.
//

import UIKit

class ColorViewController: UIViewController {
    var colorFR:String = ""
    var color:String = ""
    
    var shape = GameManager.sharedGame.drawing[GameManager.sharedGame.currentRound - 1].shape.type.description
    //var shape = "square"
    @IBOutlet weak var colorLabel: UILabel!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        colorLabel.text = self.colorFR
        
        LedManager.instance.initLedSphero()
    }
    
    func isRightColor(color:String) -> Bool {
        if self.color == color {
           return true
        }
        return false
        
    }
    
    @IBAction func onListenWordTouched(_ sender: Any) {
        Sound.play(file: "\(color).wav")
    }
    
    
    @IBAction func onColorTouched(_ sender: UIButton) {
        Sound.play(file: "\(sender.accessibilityIdentifier!).wav")
        
        
        if isRightColor(color: sender.accessibilityIdentifier!) {
            print("bonne couleure")
            switch color {
            case "red":
                switch shape {
                case "square":
                    LedManager.instance.rotateSphero(type: "correct", shape: LedManager.instance.square, color: .red)
                    Sound.play(file: "right_answer.wav")
                case "triangle":
                    LedManager.instance.rotateSphero(type: "correct", shape: LedManager.instance.triangle, color: .red)
                    Sound.play(file: "right_answer.wav")
                default:
                    LedManager.instance.rotateSphero(type: "wrong", shape: nil, color: nil)
                }
            case "blue":
                switch shape {
                case "square":
                    LedManager.instance.rotateSphero(type: "correct", shape: LedManager.instance.square, color: .blue)
                    Sound.play(file: "right_answer.wav")
                case "triangle":
                    LedManager.instance.rotateSphero(type: "correct", shape: LedManager.instance.triangle, color: .blue)
                    Sound.play(file: "right_answer.wav")
                default:
                    LedManager.instance.rotateSphero(type: "wrong", shape: nil, color: nil)
                }
            default:
                LedManager.instance.rotateSphero(type: "wrong", shape: nil, color: nil)
            }
            delayWithSeconds(4) {
                self.performSegue(withIdentifier: "toTempDraw", sender: Any?.self)
            }
            
        } else {
            LedManager.instance.rotateSphero(type: "wrong", shape: nil, color: nil)
            print("mauvaise couleur")
        }
    }
    
}
