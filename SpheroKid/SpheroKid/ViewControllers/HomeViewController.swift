//
//  ViewController.swift
//  SpheroKid
//
//  Created by Guillaume Lagouy on 11/11/2019.
//  Copyright © 2019 Guillaume Lagouy. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var btnCreate: UIButton!
    
    //var manager:SocketManager? = nil
    //var socket:SocketIOClient? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SocketSwiftManager.instance.connect(localhost: "192.168.43.146", port: "8083")
    }

    @IBAction func createGameTouched(_ sender: Any) {
        SocketSwiftManager.instance.emit(channel: "test", datas: "hello")
    }
    
}

