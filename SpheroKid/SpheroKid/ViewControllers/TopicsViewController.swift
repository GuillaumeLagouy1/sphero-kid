//
//  TopicsViewController.swift
//  SpheroKid
//
//  Created by Guillaume Lagouy on 13/11/2019.
//  Copyright © 2019 Guillaume Lagouy. All rights reserved.
//

import UIKit

class TopicsViewController: UIViewController {

    @IBOutlet weak var card1Button: UIButton!
    @IBOutlet weak var card2Button: UIButton!
    @IBOutlet weak var card3Button: UIButton!
    @IBOutlet weak var card4Button: UIButton!
    @IBOutlet weak var card5Button: UIButton!
    @IBOutlet weak var card6Button: UIButton!
    
    var allButton:[UIButton] = []
    var chosenTopic:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.allButton = [
         card1Button,
         card2Button,
         card3Button,
         card4Button,
         card5Button,
         card6Button
        ]
        
        print(GameManager.sharedGame.team)
        
        
    }
    
    @IBAction func onCardTouched(_ sender: UIButton) {
        for button in allButton {
            button.setImage(UIImage(named: "card_off"), for: .normal)
        }
        
        if sender.currentImage == UIImage(named: "card_off"){
            sender.setImage(UIImage(named: "card_on"), for: .normal)
        }
    }
    
    @IBAction func onNextTouched(_ sender: Any) {
        
        let blue = Color(type: .blue)
        let red = Color(type: .red)
        
        let square = Shape(type: .square)
        let triangle = Shape(type: .triangle)
        
        let houseBase = DrawingPart(shape: square, color: blue)
        let houseRoof = DrawingPart(shape: triangle, color: red)
        
        GameManager.sharedGame.addWordToFind(word: "MAISON")
        GameManager.sharedGame.addDrawingParts(drawingParts: [houseBase, houseRoof])
        
    }
    
}
