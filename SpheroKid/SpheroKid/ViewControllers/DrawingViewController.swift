//
//  DrawingViewController.swift
//  SpheroKid
//
//  Created by Guillaume Lagouy on 21/11/2019.
//  Copyright © 2019 Guillaume Lagouy. All rights reserved.
//

import UIKit

class DrawingViewController: UIViewController, DrawingViewDelegate {
    let shape:String = GameManager.sharedGame.drawing[GameManager.sharedGame.currentRound - 1].shape.type.description

    
    @IBOutlet weak var feedbackImageView: UIImageView!
    @IBOutlet weak var drawingView: DrawingView!
    
    private let drawnImageClassifier = DrawnImageClassifier()
    
    private var currentPrediction: DrawnImageClassifierOutput? {
        didSet {
            if let currentPrediction = currentPrediction {
                
                // display top 5 scores
                let sorted = currentPrediction.category_softmax_scores.sorted { $0.value > $1.value }
                let top5 = sorted.prefix(5)
                
                print(top5)
                
                for res in top5 {
                    if res.key == "circle" {
                        if shape == "circle" {
                            feedbackImageView.image = UIImage(named: "tick")
                            Sound.play(file: "right_answer.wav")
                            delayWithSeconds(1) {
                                self.performSegue(withIdentifier: "toColor", sender: Any?.self)
                            }
                        } else {
                            feedbackImageView.image = UIImage(named: "cross")
                            clearDrawingView()
                        }
                    }
                    if res.key == "triangle" {
                        if shape == "triangle" {
                            feedbackImageView.image = UIImage(named: "tick")
                            Sound.play(file: "right_answer.wav")
                            delayWithSeconds(1) {
                                self.performSegue(withIdentifier: "toColor", sender: Any?.self)
                            }
                        } else {
                            feedbackImageView.image = UIImage(named: "cross")
                            clearDrawingView()
                        }
                    }
                    if res.key == "square" {
                        if shape == "square" {
                            feedbackImageView.image = UIImage(named: "tick")
                            Sound.play(file: "right_answer.wav")
                            delayWithSeconds(1) {
                                self.performSegue(withIdentifier: "toColor", sender: Any?.self)
                            }
                        } else {
                            feedbackImageView.image = UIImage(named: "cross")
                            clearDrawingView()
                        }
                    }
                }
            }
            else {
                print("Waiting for drawing ...")
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        drawingView.delegate = self
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // set stroke width to 4% of the width
        let strokeWidth = drawingView.bounds.width * 0.04
        drawingView.strokeWidth = strokeWidth
    }
    
    @IBAction func clearButtonTapped(_ sender: Any) {
        clearDrawingView()
    }
    
    @IBAction func onEraserTouched(_ sender: Any) {
        clearDrawingView()
    }
    
    private func clearDrawingView() {
        drawingView.clear()
        delayWithSeconds(1) {
            self.feedbackImageView.image = nil 
        }
    }
    
    public func didEndDrawing() {
        // get image and resize it
        let image = UIImage(view: drawingView)
        let resized = image.resize(newSize: CGSize(width: 28, height: 28))
        
        guard let pixelBuffer = resized.grayScalePixelBuffer() else {
            print("couldn't create pixel buffer")
            return
        }
        
        do {
            currentPrediction = try drawnImageClassifier.prediction(image: pixelBuffer)
        }
        catch {
            print("error making prediction: \(error)")
        }
    }

}
