//
//  RecordFirstnameViewController.swift
//  SpheroKid
//
//  Created by Guillaume Lagouy on 17/11/2019.
//  Copyright © 2019 Guillaume Lagouy. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation


class RecordFirstnameViewController: UIViewController {
    var recorder = FDSoundActivatedRecorder()
    var savedURL: URL? = nil
    var player = AVPlayer()
    var strToSend:String = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let utterance = AVSpeechUtterance(string: "Dis moi, comment tu t'appelles ?")
        utterance.voice = AVSpeechSynthesisVoice(identifier: "com.apple.ttsbundle.siri_female_fr-FR_compact")
        utterance.pitchMultiplier = 1.8
        utterance.rate = 0.4

        let synthesizer = AVSpeechSynthesizer()
        //synthesizer.speak(utterance)
        
    }
    
    @IBAction func onRecordBtnTouched(_ sender: Any) {
        self.recorder.delegate = self
        self.recorder.startListening()
    }
    
    @IBAction func onPlaybackBtnTouched(_ sender: Any) {
        if let sound = savedURL {
            self.player = AVPlayer(url: sound)
            self.player.play()
        } else {
            print("no record find")
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is EditPlayerViewController
        {
            let record = self.strToSend
            let vc = segue.destination as? EditPlayerViewController
            GameManager.sharedGame.addRecord(record: record)
            //vc?.recordBase64 = record
        }
    }
    
}

extension RecordFirstnameViewController: FDSoundActivatedRecorderDelegate {
    func soundActivatedRecorderDidStartRecording(_ recorder: FDSoundActivatedRecorder) {
        print("Start recording")
    }
    
    func soundActivatedRecorderDidTimeOut(_ recorder: FDSoundActivatedRecorder) {
        print("Recording timeout")
    }
    
    func soundActivatedRecorderDidAbort(_ recorder: FDSoundActivatedRecorder) {
        print("Recording abort")
    }
    
    func soundActivatedRecorderDidFinishRecording(_ recorder: FDSoundActivatedRecorder, andSaved file: URL) {
        print("Recording finish")
        savedURL = file
        let audioFileURL = savedURL!
        let data = try! Data(contentsOf: audioFileURL)
        self.strToSend = data.base64EncodedString()
        //print(strToSend)
    }
}
