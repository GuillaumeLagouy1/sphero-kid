//
//  FillColorViewController.swift
//  SpheroKid
//
//  Created by Guillaume Lagouy on 26/11/2019.
//  Copyright © 2019 Guillaume Lagouy. All rights reserved.
//

import UIKit

class FillColorViewController: UIViewController
{
    @IBOutlet weak var colorContainerView: UIView!
    @IBOutlet weak var colorButton: UIButton!
    @IBOutlet weak var maskImageView: UIImageView!
    
    var color:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let drawing = GameManager.sharedGame.drawing[GameManager.sharedGame.currentRound - 1]
        let shape = drawing.shape.type.description
        color = drawing.color.type.description
        print(shape)
        print(color)
        colorButton.setImage(UIImage(named: "splash_\(color)"), for: .normal)
        maskImageView.image = UIImage(named: "mask_\(shape)")
    }
    
    @IBAction func onColorTouched(_ sender: UIButton) {
        switch self.color {
        case "blue":
            colorContainerView.backgroundColor = .blue
        case "red":
            colorContainerView.backgroundColor = .red
        case "green":
            colorContainerView.backgroundColor = .green
        default:
            colorContainerView.backgroundColor = .black
        }
        delayWithSeconds(1) {
            self.performSegue(withIdentifier: "toBonus", sender: Any?.self)
        }
    }
}
