//
//  SayColorViewController.swift
//  SpheroKid
//
//  Created by Guillaume Lagouy on 27/11/2019.
//  Copyright © 2019 Guillaume Lagouy. All rights reserved.
//

import UIKit
import Speech

class SayColorViewController: UIViewController {
    
    @IBOutlet weak var startRecordButton: UIButton!
    @IBOutlet weak var maskShapeImageView: UIImageView!
    @IBOutlet weak var feedbackImageView: UIImageView!
    
    var color:String = GameManager.sharedGame.drawing[GameManager.sharedGame.currentRound - 1].color.getTraduction()
    var colors:[String] = ["rouge", "vert", "bleu"]
    var shape:String = GameManager.sharedGame.drawing[GameManager.sharedGame.currentRound - 1].shape.type.description
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //color = "rouge"
        maskShapeImageView.image = UIImage(named: "mask_black_\(shape)")
    }
    
    var isRecording:Bool = false
    
    @IBAction func onStartRecordingTouched(_ sender: Any) {
        if(isRecording){
            print("arrêt de l'enregistrement")
            self.isRecording = false
            
        } else {
            /*
            AudioManager.audioInstance.recognizeSpeech { (strArray) in
                print(strArray)
                for color in self.colors {
                    if strArray.last == color {
                        if color == self.color {
                            self.feedbackImageView.image = UIImage(named: "tick")
                            delayWithSeconds(2) {
                                self.performSegue(withIdentifier: "toFillColor", sender: Any?.self)
                            }
                        } else {
                            self.feedbackImageView.image = UIImage(named: "cross")
                            delayWithSeconds(1) {
                                self.feedbackImageView.image = nil
                            }
                        }
                    }
                }
            }*/
            self.isRecording = true
            startRecordButton.isEnabled = false
        }
    }
    
    @IBAction func onWrongTouched(_ sender: Any) {
        self.feedbackImageView.image = UIImage(named: "cross")
        delayWithSeconds(1) {
            self.feedbackImageView.image = nil
        }
    }
    
    @IBAction func onRightTouched(_ sender: Any) {
        self.feedbackImageView.image = UIImage(named: "tick")
        Sound.play(file: "right_answer.wav")
        delayWithSeconds(1) {
            self.performSegue(withIdentifier: "toFillColor", sender: Any?.self)
        }
    }
    
    
}
