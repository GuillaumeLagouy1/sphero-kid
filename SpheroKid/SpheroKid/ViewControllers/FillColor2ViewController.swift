//
//  FillColor2ViewController.swift
//  SpheroKid
//
//  Created by Guillaume Lagouy on 27/11/2019.
//  Copyright © 2019 Guillaume Lagouy. All rights reserved.
//

import UIKit

class FillColor2ViewController: UIViewController {
    
    var shape:String = GameManager.sharedGame.drawing[GameManager.sharedGame.currentRound - 1].shape.type.description
    var color:String = GameManager.sharedGame.drawing[GameManager.sharedGame.currentRound - 1].color.type.description
    
    @IBOutlet weak var colorButton: UIButton!
    @IBOutlet weak var maskShapeImageView: UIImageView!
    @IBOutlet weak var colorView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        maskShapeImageView.image = UIImage(named: "mask_black_\(shape)")
        colorButton.setImage(UIImage(named: "splash_\(color)"), for: .normal)
    }
    @IBAction func onColorTouched(_ sender: Any) {
        switch color {
        case "red":
            colorView.backgroundColor = .red
            Sound.play(file: "right_answer.wav")
            delayWithSeconds(1) {
                 self.performSegue(withIdentifier: "toBonus", sender: Any?.self)
            }
        case "blue":
            colorView.backgroundColor = .blue
            Sound.play(file: "right_answer.wav")
            delayWithSeconds(1) {
                 self.performSegue(withIdentifier: "toBonus", sender: Any?.self)
            }
        case "green":
            colorView.backgroundColor = .green
            Sound.play(file: "right_answer.wav")
            delayWithSeconds(1) {
                 self.performSegue(withIdentifier: "toBonus", sender: Any?.self)
            }
        default:
            colorView.backgroundColor = .white
        }
    }
}
