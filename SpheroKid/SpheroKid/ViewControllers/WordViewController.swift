//
//  WordViewController.swift
//  SpheroKid
//
//  Created by Guillaume Lagouy on 18/11/2019.
//  Copyright © 2019 Guillaume Lagouy. All rights reserved.
//

import UIKit

class WordViewController: UIViewController {

    @IBOutlet weak var wordCollectionView: UICollectionView!
    @IBOutlet weak var lettersCollectionView: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    
    let wordToFind = GameManager.sharedGame.wordToFind.map { String($0) }
    var letterFound:[String] = GameManager.sharedGame.lettersFound
    
    //let wordToFind = ["M", "A", "I", "S", "O","N"]
    //var letterFound = ["A", "N"]
    
    var word:[String] = []
    var letter:[String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        wordCollectionView.delegate = self
        wordCollectionView.dataSource = self
        
        lettersCollectionView.delegate = self
        lettersCollectionView.dataSource = self
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
        //layout.itemSize = CGSize(width: screenWidth/3, height: screenWidth/3)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 5
        layout.scrollDirection = .horizontal
        let layout1: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout1.sectionInset = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
        //layout.itemSize = CGSize(width: screenWidth/3, height: screenWidth/3)
        layout1.minimumInteritemSpacing = 0
        layout1.minimumLineSpacing = 5
        layout1.scrollDirection = .horizontal
        wordCollectionView!.collectionViewLayout = layout
        lettersCollectionView!.collectionViewLayout = layout1
        
        //print(letterFound)
        
        
        let missingLetters = WordManager.instance.getMissingLetters(wordToFind: self.wordToFind, lettersFound: self.letterFound)
        self.letter = WordManager.instance.generateLettersToChoice(missingLetters: missingLetters, count: 3)
    }
    
    func celebration() {
        titleLabel.text = "BRAVO ! TU AS DEVINÉ MON DESSIN"
        SocketSwiftManager.instance.emit(channel: "finalDrawing", datas: "display drawing")
    }
}

extension WordViewController:UICollectionViewDelegate{
    
}

extension WordViewController:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == wordCollectionView {
            return self.word.count
        }
        if collectionView == lettersCollectionView  {
            return self.letter.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        self.word = WordManager.instance.buildWordForView(wordToFind: wordToFind, lettersFound: letterFound)
        
        //print(wordToFind)
        //print(letterFound)
        
        //let wordToFindSorted = wordToFind.sorted()
        //let letterFoundSorted = letterFound.sorted()
        //print(wordToFindSorted)
        //print(letterFoundSorted)
        print()
        if WordManager.instance.getMissingLetters(wordToFind: self.wordToFind, lettersFound: self.letterFound).count == 0 {
            celebration()
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "letterCell", for: indexPath) as! LetterCollectionViewCell
        
        if collectionView == wordCollectionView {
            if word[indexPath.row] == "missingLetter" {
                cell.letterImageView!.image = UIImage(named: "missingLetter")
            } else {
                cell.letterImageView!.image = UIImage(named: "\(word[indexPath.row])_yellow")
            }
        }
        
        else {
    
            cell.letterImageView!.image = UIImage(named: "\(letter[indexPath.row])_white")
            
        }
            
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == lettersCollectionView {
            //print(letter[indexPath.row])
            
            if wordToFind.contains(letter[indexPath.row]){
                self.letterFound.append(letter[indexPath.row])
            }
            
            
            let missingLetters = WordManager.instance.getMissingLetters(wordToFind: self.wordToFind, lettersFound: self.letterFound)
            self.letter = WordManager.instance.generateLettersToChoice(missingLetters: missingLetters, count: 3)
            
            reloadCollectionView()
        }
    }
    
    func reloadCollectionView() {
        print("regenerate")
        wordCollectionView.reloadData()
        lettersCollectionView.reloadData()
    }
}

extension WordViewController : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        return CGSize(width: 100, height: collectionView.frame.height)
    }
}


