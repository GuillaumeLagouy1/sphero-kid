//
//  ShapeViewController.swift
//  SpheroKid
//
//  Created by Guillaume Lagouy on 24/11/2019.
//  Copyright © 2019 Guillaume Lagouy. All rights reserved.
//

import UIKit

class ShapeViewController: UIViewController {

    var shapeFR:String = ""
    var shape:String = ""
    
    @IBOutlet weak var shapeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        shapeLabel.text = shapeFR
        LedManager.instance.initLedSphero()
    }
    
    func isRightShape(shape:String) -> Bool {
        if self.shape == shape {
           return true
        }
        return false
        
    }
    
    @IBAction func onListenWordTouched(_ sender: Any) {
        Sound.play(file: "\(shape).wav")
    }
    
    
    
    @IBAction func onShapeTouched(_ sender: UIButton) {
        Sound.play(file: "\(sender.accessibilityIdentifier!).wav")
        
        
        if isRightShape(shape: sender.accessibilityIdentifier!) {
            print("bonne shape")
            switch shape {
            case "square":
                LedManager.instance.rotateSphero(type: "correct", shape: LedManager.instance.square, color: .white)
                Sound.play(file: "right_answer.wav")
            case "triangle":
                LedManager.instance.rotateSphero(type: "correct", shape: LedManager.instance.triangle, color: .white)
                Sound.play(file: "right_answer.wav")
            default:
                LedManager.instance.rotateSphero(type: "correct", shape: nil, color: nil)
                Sound.play(file: "right_answer.wav")
            }
            
            delayWithSeconds(5) {
                self.performSegue(withIdentifier: "toTempDraw", sender: Any?.self)
            }
            
        } else {
            LedManager.instance.rotateSphero(type: "wrong", shape: nil, color: nil)
            print("mauvaise shape")
        }
    }
}
