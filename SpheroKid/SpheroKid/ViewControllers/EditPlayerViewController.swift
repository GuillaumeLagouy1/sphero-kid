//
//  EditPlayerViewController.swift
//  SpheroKid
//
//  Created by Guillaume Lagouy on 15/11/2019.
//  Copyright © 2019 Guillaume Lagouy. All rights reserved.
//

import UIKit

var firstname:String = ""
class EditPlayerViewController: UIViewController {
    
    @IBOutlet weak var firstnameTextField: UITextField!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var firstnameLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var createPlayerButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        if GameManager.sharedGame.pictureBase64 != "" {
            avatarImageView.image = convertBase64ToImage(imageString: GameManager.sharedGame.pictureBase64)
        }
        
        print(firstname)
        
        if firstname != "" {
            firstnameLabel.text = firstname
                    
        }
        
        createPlayerButton.isEnabled = false
        if firstname != "" && GameManager.sharedGame.pictureBase64 != "" && GameManager.sharedGame.recordBase64 != "" {
            createPlayerButton.isEnabled = true
        }
        
        // VISUEL INPUT
        firstnameTextField.backgroundColor = .clear
        firstnameTextField.layer.cornerRadius = 15.0
        firstnameTextField.layer.borderWidth = 10.0
        firstnameTextField.layer.borderColor = UIColor.white.cgColor
        firstnameTextField.setLeftPaddingPoints(20)
    }
    
    @IBAction func onFirstnameCreateTouched(_ sender: Any) {
        if let text = firstnameTextField.text {
            firstname = text
            firstnameLabel.text = text
        }
    }
    
    @IBAction func onEditingDidEnd(_ sender: Any) {
        firstname = firstnameLabel.text!
    }
    
    
    @IBAction func onCreatePlayerTouched(_ sender: Any) {
        print(firstname)
        if firstname != "" && GameManager.sharedGame.pictureBase64 != "" && GameManager.sharedGame.recordBase64 != "" {
            print("add player")
            let player = Player(firstname: firstname, avatar: GameManager.sharedGame.pictureBase64, record: GameManager.sharedGame.recordBase64)
            
            GameManager.sharedGame.addPlayer(player: player)
            
            let avatar = getDumpAvatar()
            GameManager.sharedGame.addPlayer(player: Player(firstname: "Alberic", avatar: avatar, record: getAlbericRecord()))
            GameManager.sharedGame.addPlayer(player: Player(firstname: "Christophe", avatar: avatar, record: getChristopheRecord()))
            GameManager.sharedGame.addPlayer(player: Player(firstname: "Leah", avatar: avatar, record: getLeahRecord()))
            GameManager.sharedGame.addPlayer(player: Player(firstname: "Yann", avatar: avatar, record: getYannRecord()))
            
            //reset()
            performSegue(withIdentifier: "toTeam", sender: Any?.self)
        } else {
            print("The player's name or avatar is missing")
        }
    }
    
    @IBAction func onAdd5PlayersTouched(_ sender: Any) {
        let avatar = getDumpAvatar()
        /*let record = getDumpRecord()
        //GameManager.sharedGame.addPlayer(player: Player(firstname: "toto", avatar: avatar, record: record))
        GameManager.sharedGame.addPlayer(player: Player(firstname: "titi", avatar: avatar, record: record))
        GameManager.sharedGame.addPlayer(player: Player(firstname: "tutu", avatar: avatar, record: record))
        GameManager.sharedGame.addPlayer(player: Player(firstname: "tata", avatar: avatar, record: record))
        GameManager.sharedGame.addPlayer(player: Player(firstname: "tata", avatar: avatar, record: record))*/
    }
    
    func reset() {
        // reset firstname
        firstnameLabel.text = "NOM"
        // reset image
        avatarImageView.image = UIImage(named: "picture_frame")
    }
    
    @IBAction func onEditingChanged(_ sender: Any) {
        firstnameLabel.text = firstnameTextField.text
    }
    
    
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
