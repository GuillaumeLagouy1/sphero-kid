//
//  WakeupViewController.swift
//  SpheroKid
//
//  Created by Guillaume Lagouy on 26/11/2019.
//  Copyright © 2019 Guillaume Lagouy. All rights reserved.
//

import UIKit

class WakeupViewController: UIViewController {

    @IBOutlet weak var wakeupButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func onWakeupTouched(_ sender: Any) {
        if GameManager.sharedGame.currentRound < 2 {
            wakeupButton.isEnabled = false
            SocketSwiftManager.instance.emit(channel: "spheroDrawing", datas: "display sphero screen")
            delayWithSeconds(3) {
                GameManager.sharedGame.incrementRound()
                self.performSegue(withIdentifier: "toDrawing", sender: Any?.self)
            }
            
            
            
        } else {
            //AudioManager.audioInstance.stopRecording()
            SocketSwiftManager.instance.emit(channel: "spheroDrawing", datas: "display sphero screen")
            wakeupButton.isEnabled = false
            delayWithSeconds(3) {
                self.performSegue(withIdentifier: "toGuessWord", sender: Any?.self)
            }
        }
    }
}
