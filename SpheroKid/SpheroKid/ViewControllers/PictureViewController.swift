//
//  PictureViewController.swift
//  SpheroKid
//
//  Created by Guillaume Lagouy on 16/11/2019.
//  Copyright © 2019 Guillaume Lagouy. All rights reserved.
//

import UIKit
import AVFoundation


var avatarBase64 = ""
class PictureViewController: UIViewController {
    
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var previewAvatarView: UIView!
    @IBOutlet weak var imgTest: UIImageView!
    @IBOutlet weak var resultPicture: UIImageView!
    
    
    let cameraManager = CameraManager()
    var avatarBase64 = "";

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.cameraManager.cameraDevice = .front
        self.cameraManager.shouldFlipFrontCameraImage = true
        self.cameraManager.cameraOutputQuality = .medium
        self.cameraManager.writeFilesToPhoneLibrary = false
        self.cameraManager.addPreviewLayerToView(previewView)

    }

    @IBAction func onTakePictureTouched(_ sender: Any) {
        self.cameraManager.capturePictureWithCompletion({ result in
            switch result {
                case .failure:
                    print("error")
                case .success(let content):
                    self.resultPicture.transform = self.resultPicture.transform.rotated(by: .pi)
                    self.resultPicture.image = content.asImage
            }
        })
        
        delayWithSeconds(1.5) {
            let avatarImg = self.previewAvatarView.screenshot()
            self.imgTest.image =  avatarImg
            
            self.avatarBase64 = convertImageToBase64(image: avatarImg)
        }
        
    }
    
    func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is EditPlayerViewController
        {
            let vc = segue.destination as? EditPlayerViewController
            GameManager.sharedGame.addPicture(picture: self.avatarBase64)
        }
    }
}

extension UIView {
    func screenshot() -> UIImage {
        if #available(iOS 10.0, *) {
            return UIGraphicsImageRenderer(size: bounds.size).image { _ in
                drawHierarchy(in: CGRect(origin: .zero, size: bounds.size), afterScreenUpdates: true)
            }
        } else {
            UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
            drawHierarchy(in: self.bounds, afterScreenUpdates: true)
            let image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
            UIGraphicsEndImageContext()
            return image
        }
    }
}
