//
//  SayShapeViewController.swift
//  SpheroKid
//
//  Created by Guillaume Lagouy on 27/11/2019.
//  Copyright © 2019 Guillaume Lagouy. All rights reserved.
//

import Foundation
import UIKit
import Speech

class SayShapeViewController: UIViewController, SFSpeechRecognizerDelegate {
    
    @IBOutlet weak var startRecordButton: UIButton!
    @IBOutlet weak var shapeResultLabel: UILabel!
    @IBOutlet weak var feedbackImageView: UIImageView!
    
    var isRecording:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    

    let shape:String = GameManager.sharedGame.drawing[GameManager.sharedGame.currentRound - 1].shape.getTraduction()
    //let shape = "carré"
    let shapes:[String] = ["cercle", "carré", "triangle"]
    
    @IBAction func onWrongTouched(_ sender: Any) {
        self.feedbackImageView.image = UIImage(named: "cross")
        delayWithSeconds(1) {
            self.feedbackImageView.image = nil
        }
    }
    
    
    @IBAction func onRightTouched(_ sender: Any) {
        self.feedbackImageView.image = UIImage(named: "tick")
        Sound.play(file: "right_answer.wav")
        delayWithSeconds(1) {
            self.performSegue(withIdentifier: "toDrawing", sender: Any?.self)
        }
    }
    
    
    @IBAction func onStartRecordingTouched(_ sender: Any) {
        
        
        if(isRecording){
            // STOP
            print("arrêt de l'enregistrement")
            self.isRecording = false
            
        } else {
            /*
            AudioManager.audioInstance.start()
            
            AudioManager.audioInstance.recognizeSpeech { (strArray) in
                //print(strArray)
                for shape in self.shapes {
                    if strArray.last == shape {
                        if shape == self.shape {
                            self.feedbackImageView.image = UIImage(named: "tick")
                            delayWithSeconds(2) {
                                self.performSegue(withIdentifier: "toDrawing", sender: Any?.self)
                            }
                        } else {
                            self.feedbackImageView.image = UIImage(named: "cross")
                            delayWithSeconds(1) {
                                self.feedbackImageView.image = nil
                            }
                        }
                    }
                }
                
            }*/
            self.isRecording = true
            startRecordButton.isEnabled = false
        }
    }
    
}
