//
//  BonusLetterViewController.swift
//  SpheroKid
//
//  Created by Guillaume Lagouy on 27/11/2019.
//  Copyright © 2019 Guillaume Lagouy. All rights reserved.
//

import UIKit

class BonusLetterViewController: UIViewController {

    @IBOutlet weak var bonusLetterImageView: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Sound.play(file: "win-letter.wav")
        let wordToFind = GameManager.sharedGame.wordToFind.map { String($0) }
        let lettersFound = GameManager.sharedGame.lettersFound
        
        let missingLetters = WordManager.instance.getMissingLetters(wordToFind: wordToFind, lettersFound: lettersFound)
        let bonusLetter = pickRandomLetter(missingLetters: missingLetters)
        GameManager.sharedGame.addLetterFound(letter: bonusLetter)
        bonusLetterImageView.image = UIImage(named: "\(bonusLetter)_yellow")
        delayWithSeconds(3) {
            self.performSegue(withIdentifier: "toWakeup", sender: Any?.self)
        }
    }
    
    func pickRandomLetter(missingLetters:[String]) -> String {
        return missingLetters[Int(arc4random_uniform(UInt32(missingLetters.count)) + 0)]
    }
}
