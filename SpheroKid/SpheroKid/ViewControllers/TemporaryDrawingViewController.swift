//
//  TemporaryDrawingViewController.swift
//  SpheroKid
//
//  Created by Guillaume Lagouy on 24/11/2019.
//  Copyright © 2019 Guillaume Lagouy. All rights reserved.
//

import UIKit

var hasColor:Bool = false
var hasShape:Bool = false
var count:Int = 0

class TemporaryDrawingViewController: UIViewController {
    let shape:String = GameManager.sharedGame.drawing[GameManager.sharedGame.currentRound - 1].shape.type.description
    let shapeFR:String = GameManager.sharedGame.drawing[GameManager.sharedGame.currentRound - 1].shape.getTraduction()
    let color:String = GameManager.sharedGame.drawing[GameManager.sharedGame.currentRound - 1].color.type.description
    let colorFR:String = GameManager.sharedGame.drawing[GameManager.sharedGame.currentRound - 1].color.getTraduction()
    
    var team:[String] = []
    var teamJSON:[String] = []
    
    @IBOutlet weak var toShapeButton: UIButton!
    @IBOutlet weak var toColorButton: UIButton!
    @IBOutlet weak var colorLabel: UILabel!
    @IBOutlet weak var shapeLabel: UILabel!
    @IBOutlet weak var bannerImageView: UIImageView!
    @IBOutlet weak var drawButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        LedManager.instance.initLedSphero()
        delayWithSeconds(15) {
            LedManager.instance.resetSpheroLed()
        }
        
        // BANNER ================================
        bannerImageView.image = UIImage(named: "banner_round_\(GameManager.sharedGame.currentRound)")!
        // =======================================
        
        for player in GameManager.sharedGame.team {
            let jsonEncoder = JSONEncoder()
            let jsonData = try! jsonEncoder.encode(player)
            let json = String(data: jsonData, encoding: String.Encoding.utf8)
            teamJSON.append(json!)
        }
        
        for player in GameManager.sharedGame.team {
            team.append(player.firstname)
        }
        
        toShapeButton.isEnabled = hasShape
        toColorButton.isEnabled = hasColor
        
        if hasColor {
            colorLabel.text = colorFR
        }
        
        if hasShape {
            shapeLabel.text = shapeFR
        }
        
        if hasShape && hasColor {
            drawButton.isEnabled = true
        } else {
            drawButton.isEnabled = false
        }

        // SOCKET
        // Déjà connecté dans la home
        
        // SOCKET MOBILE =======================================
        print("socket mobile")
        SocketSwiftManager.instance.listenToChannel(channel: "action") { receivedStr in
            if let str = receivedStr {
                switch str {
                case "role":
                    SocketSwiftManager.instance.emit(channel: "players", datas: self.teamJSON)
                case "shape":
                    SocketSwiftManager.instance.emit(channel: "shape", datas: ["shape": self.shape, "duration": 5 ])
                case "color":
                    SocketSwiftManager.instance.emit(channel: "color", datas: ["color": self.color, "duration": 5 ])
                case "sphero":
                    self.connectSphero()
                default:
                    print("erreur")
                }
                
            } else{
                print("Received wrong type")
            }
        
        }

        
        // =====================================================
        
        SharedToyBox.instance.bolt?.setCollisionDetection(configuration: .enabled)
        SharedToyBox.instance.bolt?.onCollisionDetected = { collisionData in
            DispatchQueue.main.sync {
                self.activateButton()
            }
        }
    }
    @IBAction func onRevealShapeTouched(_ sender: Any) {
        SocketSwiftManager.instance.emit(channel: "shape", datas: ["shape": shape, "duration": 5 ])
    }
    
    @IBAction func onRevealColorTouched(_ sender: Any) {
        SocketSwiftManager.instance.emit(channel: "color", datas: ["color": color, "duration": 5 ])
    }
    
    @IBAction func onConnectTouched(_ sender: Any) {
        // SPHERO
        SharedToyBox.instance.searchForBoltsNamed(["SB-1219"]) { err in
            if err == nil {
                print("Sphero connected")
                SharedToyBox.instance.bolt?.setCollisionDetection(configuration: .enabled)
                SharedToyBox.instance.bolt?.onCollisionDetected = { collisionData in
                    DispatchQueue.main.sync {
                        self.activateButton()
                    }
                }
            }
        }
    }
    @IBAction func onDisplayRoleTouched(_ sender: Any) {
        
        SocketSwiftManager.instance.emit(channel: "players", datas: teamJSON)
    }
    
    func connectSphero(){
        // SPHERO
        SharedToyBox.instance.searchForBoltsNamed(["SB-1219"]) { err in
            if err == nil {
                print("Sphero connected")
                SharedToyBox.instance.bolt?.setCollisionDetection(configuration: .enabled)
                SharedToyBox.instance.bolt?.onCollisionDetected = { collisionData in
                    DispatchQueue.main.sync {
                        self.activateButton()
                    }
                }
            }
        }
    }
    
    func activateButton() {
        print("Secousse !")
        print(count)
        if count == 0 {
            toShapeButton.isEnabled = true
            count += 1
        } else {
            toColorButton.isEnabled = true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is ColorViewController
        {
            hasColor = true
            let vc = segue.destination as? ColorViewController
            vc?.colorFR = self.colorFR
            vc?.color = self.color
        }
        
        if segue.destination is ShapeViewController
        {
            hasShape = true
            let vc = segue.destination as? ShapeViewController
            vc?.shapeFR = self.shapeFR
            vc?.shape = self.shape
        }
        
        if segue.destination is SayShapeViewController
        {
            print("--------------------")
            print("Reset de l'affichage")
            print("--------------------")
            reset()
        }
    }
    
    func reset() {
        hasColor = false
        hasShape = false
        
        colorLabel.text = "COULEUR ?"
        shapeLabel.text = "FORME ?"
        
        // reset le count
        count = 0
    }
}
