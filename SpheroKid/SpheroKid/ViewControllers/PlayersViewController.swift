//
//  PlayersViewController.swift
//  SpheroKid
//
//  Created by Guillaume Lagouy on 13/11/2019.
//  Copyright © 2019 Guillaume Lagouy. All rights reserved.
//

import UIKit

class PlayersViewController: UIViewController {
    
    class PlayerUI {
        let avatarElmt:UIButton
        let firstnameElmt:UILabel
        
        init(_ avatarElmt:UIButton, _ firstnameElmt:UILabel) {
            self.avatarElmt = avatarElmt
            self.firstnameElmt = firstnameElmt
        }
    }
    
    @IBOutlet weak var nextButton: UIButton!
    
    // Player avatar
    @IBOutlet weak var player1Button: UIButton!
    @IBOutlet weak var player2Button: UIButton!
    @IBOutlet weak var player3Button: UIButton!
    @IBOutlet weak var player4Button: UIButton!
    @IBOutlet weak var player5Button: UIButton!
    
    // Player firstname
    @IBOutlet weak var firstnamePlayer1Label: UILabel!
    @IBOutlet weak var firstnamePlayer2Label: UILabel!
    @IBOutlet weak var firstnamePlayer3Label: UILabel!
    @IBOutlet weak var firstnamePlayer4Label: UILabel!
    @IBOutlet weak var firstnamePlayer5Label: UILabel!
    
    var PlayerUIs:[PlayerUI] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // DUMP players ===================================================================
        /*
        let avatar = getDumpAvatar()
        let record = getDumpRecord()
        
        GameManager.sharedGame.addPlayer(player: Player(firstname: "tete", avatar: avatar, record: record))
        GameManager.sharedGame.addPlayer(player: Player(firstname: "toto", avatar: avatar, record: record))
        GameManager.sharedGame.addPlayer(player: Player(firstname: "titi", avatar: avatar, record: record))
        GameManager.sharedGame.addPlayer(player: Player(firstname: "tutu", avatar: avatar, record: record))
        GameManager.sharedGame.addPlayer(player: Player(firstname: "tata", avatar: avatar, record: record))
        */
        // ================================================================================
        print(GameManager.sharedGame.team)
        let players = GameManager.sharedGame.team
        
        
        if players.count < 5 {
            nextButton.isEnabled = false
        } else {
            nextButton.isEnabled = true
        }
        
        // Ajout des joueurs dans la vue
        self.PlayerUIs = [
            PlayerUI(player1Button, firstnamePlayer1Label),
            PlayerUI(player2Button, firstnamePlayer2Label),
            PlayerUI(player3Button, firstnamePlayer3Label),
            PlayerUI(player4Button, firstnamePlayer4Label),
            PlayerUI(player5Button, firstnamePlayer5Label)
        ]
        
        
        
        if players.isEmpty == false {
            for n in 0...players.count - 1 {
                PlayerUIs[n].avatarElmt.setImage(convertBase64ToImage(imageString: players[n].avatar), for: .normal)
                PlayerUIs[n].firstnameElmt.text = players[n].firstname
            }
        } else {
            print("Aucun joueur")
        }
    }
}

